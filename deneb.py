####
# title: deneb.py
#
# language: python3
# date: 201181119
# author: bue
#
# run:
#    from deneb import deneb
#    import deneb.deneb as den
#
# description:
#    altair based plotting in pandas
#    note: this library (with exceptio of the tidy function) is no longer needed,
#    as soon it is possible to make boxplot straight outta altair,
#    as soon altair utilizes vega3 as default backend.
###

# libraries
import altair as alt
import pandas as pd
import sys

def tidy(df_unstacked, s_value="value"):
    """
    transforms a simple unstacked dataframe into a tidy one
    """
    if (df_unstacked.index.name == None):
        sys.exit("Error @ deneb.tidy : df_unstacked.index.name missing.")
    if (df_unstacked.columns.name == None):
        sys.exit("Error @ deneb.tidy : df_unstacked.columns.name missing.")
    # transform
    df_tidy = df_unstacked.stack().reset_index()
    l_column = list(df_tidy.columns)
    l_column[l_column.index(0)] = s_value
    df_tidy.columns = l_column
    # output
    return(df_tidy)


# deneb library
def boxh(
        df_tidy,
        x = None,
        x_type = "quantitative", #:Q
        x_axis_title = None,
        y = None,
        y_type = "nominal", #:N :O
        y_sort = "ascending",
        color_box = "#4682B4", #"#4C78A8"
        color_mean = "cyan",
        color_median = "yellow",
    ):
    """
    input:
        df_tidy: tidy pandas data frame.
        x: column name x axis data.
        x_type: vega data type x axis data,
            valid vega data types are nominal or ordinal or quantitative.
        x_axis_title: x axis label.
        y: column name y axis data.
        y_type: vega data type y axis data.
            valid vega data types are nominal or ordinal or quantitative.
        y_sort: box sorting. possible values are 'ascending', 'descending'
            or an altair Sort object like
            alt.Sort(field="x", op="mean", order="descending").
        color_*: either any valid HTML color string
            or an altair Color object like
            alt.Color(field="y", type="nominal").

    output:
        altair Chart object


    description:
        altair boxplot horizontal wrapper function
    """
    # bue 20181114: check for any none
    if (x is None) or (x_axis_title is None) or (y is None):
        sys.exit("Error @ deneb.boxh : x or x_axis_title or y is still None")

    # y axis channel
    o_y = alt.Y(
        y,
        type = y_type,
        sort = y_sort,
        axis=alt.Axis(
            orient="left",
        ),
    )
    o_y_hack = alt.Y(
        y,
        type = y_type,
        sort = y_sort,
        axis=alt.Axis(
            labels=False,
            ticks=False,
            title=None,
        ),
    )

    # x axis channels
    o_x_lower_whisker =  alt.X(
        x,
        type = x_type,
        aggregate = "min",
    )
    o_x_lower_box = alt.X(
        x,
        type = x_type,
        aggregate = "q1",
    )
    o_x_upper_box = alt.X(
        x,
        type = x_type,
        aggregate = "q3",
    )
    o_x_upper_whisker =  alt.X(
        x,
        type = x_type,
        aggregate = "max",
    )
    o_x_mean = alt.X(
        x,
        type = x_type,
        aggregate = "mean",
        axis = alt.Axis(
            title=x_axis_title,
            orient="top",
            offset=4,
        ),
    )
    o_x_median = alt.X(
        x,
        type = x_type,
        aggregate = "median",
    )

    # plot elements
    o_lower_whisker = alt.Chart(df_tidy).mark_rule().encode(
        x = o_x_lower_whisker,
        x2 = o_x_lower_box,
        y = o_y_hack,
    )
    if (type(color_box) == str):
        o_middle_box = alt.Chart().mark_bar(color=color_box, size=18).encode(
            x = o_x_lower_box,
            x2 = o_x_upper_box,
            y = o_y_hack,
        )
    else:
        o_middle_box = alt.Chart().mark_bar(size=18).encode(
            x = o_x_lower_box,
            x2 = o_x_upper_box,
            y = o_y_hack,
            color = color_box,
        )
    o_upper_whisker = alt.Chart(df_tidy).mark_rule().encode(
        x = o_x_upper_whisker,
        x2 = o_x_upper_box,
        y = o_y_hack,
    )
    if (type(color_mean) == str):
        o_mean_tick = alt.Chart().mark_tick(color=color_mean).encode(
            x = o_x_mean,
            y = o_y,
        )
    else:
        o_mean_tick = alt.Chart().mark_tick().encode(
            x = o_x_mean,
            y = o_y,
            color = color_mean,
        )
    if (type(color_median) == str):
        o_median_tick = alt.Chart(df_tidy).mark_tick(color=color_median).encode(
            x = o_x_median,
            y = o_y_hack,
        )
    else:
        o_median_tick = alt.Chart(df_tidy).mark_tick().encode(
            x = o_x_median,
            y = o_y_hack,
            color = color_median,
        )
    # plot layout
    o_chart = alt.layer(
        o_lower_whisker,
        o_middle_box,
        o_upper_whisker,
        o_mean_tick,
        o_median_tick,
        data=df_tidy,
    ).resolve_scale(y='independent') # hack
    # output
    return(o_chart)


def heat(
        df_tidy,
        x = None,
        x_type = ":O",
        x_sort = "ascendic",
        y = None,
        y_type = ":O",
        y_sort = "ascendic",
        color = None,
        color_type = ":Q",
    ):
    """
    description:

    """
    o_chart = alt.Chart(df_tidy).mark_rect().encode(
        x=f"{x}{x_type}",
        y=f"{y}{y_type}",
        color=f"{color}{color_type}",
    )
    # output
    return(o_chart)

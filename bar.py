# libraries
import altair as alt
import pandas as pd


# generate data
data = pd.DataFrame({
    'a': list('CCCDDDEEE'),
    'b': [2, 7, 4, 1, 2, 6, 8, 4, 7]
})

# bar plot
alt.Chart(data).mark_bar().encode(
    x='a',
    y='average(b)'
)

# bar plot
alt.Chart(data).mark_bar().encode(
    x=alt.X(
        'a',
    ),
    y=alt.Y(
        'b',
        aggregate='mean',
    ),
)

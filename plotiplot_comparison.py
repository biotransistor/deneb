####
# title: comparision of pandas plot and cufflinks iplot function.
#
# detailed information can be found here:
# + https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.plot.html
# + https://pandas.pydata.org/pandas-docs/stable/visualization.html
# + https://github.com/santosjorge/cufflinks
# + help(df.plot)
# + help(df.iplot)
#
####

# library
import cufflinks as cf
cf.go_offline()
import matplotlib.pyplot as plt
import pandas as pd

# tidy dataframe
df = pd.DataFrame({
    'a':[1,2,3,4,2.5,2.5,2.5, 2.5],
    'b':[1,2,3,4,1,2,3,4],
    'c':['i','i','i','i','j','j','j','j']
})

# pandas matplotlib plot example
ax = df.loc[df.c.isin({'i'}),:].plot(
    kind="scatter",
    x="b",
    y="a",
    title="plot_scatter",
    color="orange",
)
df.loc[df.c.isin({'j'}),:].plot(
    kind="scatter",
    x="b",
    y="a",
    color="blue",
    ax=ax
)
plt.tight_layout()
plt.savefig("plot_scatter.png")

# pandas cufflinks iplot example
df.iplot(
    kind="scatter",
    x="b",
    y="a",
    categories='c',
    title="iplot_scatter",
    xTitle="b",
    yTitle="a",
    asPlot=True
)
